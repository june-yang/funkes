# -*- coding: UTF-8 -*-

from django.core.mail import EmailMessage
from django.conf import settings
from django.template.loader import render_to_string

class Mail(EmailMessage):

    mail_content = None

    def __init__(self, mail_subject, mail_to, mail_cc, mail_content):
        if not isinstance(mail_subject, str):
            assert 'mail_subject type error'

        if not isinstance(mail_to, list):
            assert 'mail_to type error'

        if not isinstance(mail_cc, list):
            assert 'mail_cc type error'

        if not isinstance(mail_content, MailContent):
            assert 'mail_content type error'
        self.mail_content = mail_content
        # default rainbow send mail with html type
        self.content_subtype = 'html'

        super(Mail, self).__init__(subject=mail_subject,
                                   body=mail_content.render(),
                                   from_email=settings.DEFAULT_FROM_EMAIL,
                                   to=mail_to,
                                   cc=mail_cc)


class MailContent(object):

    header = None
    body = None
    extra = None
    default_log = settings.STATIC_URL + 'dist/img/ly_com.png'

    def __init__(self, header, body, extra=None):
        if isinstance(body, list) and len(body) > 0:
            self.body = body
        else:
            assert 'body type error'

        if isinstance(header, str):
            self.header = header
        else:
            assert 'header type error'

        if extra:
            self.extra = extra

    def render(self):
        content = render_to_string('index/_email_template.html',
                                   {'header': self.header,
                                    'body': self.body,
                                    'extra': self.extra,
                                    'ly_com': self.default_log})
        return content

class MailText(object):

    size = 12
    weight = 400
    color = '#58666e'
    text = None
    extra = None

    def __init__(self, text, size=None, color=None, extra=None):
        self.text = text

        if size:
            self.size = size
        if color:
            self.color = color
        if extra:
            self.extra = extra
