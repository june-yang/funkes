# -*- coding: UTF-8 -*-
from django.conf.urls import url
from django.conf.urls import patterns

urlpatterns = patterns(
    'website.index.views',
    url(r'^$', 'index', name='index'),
    url(r'^detail/(.+)/$', 'detail', name='detail'),
    url(r'^signin$', 'signin', name='signin'),
    url(r'^apply$', 'apply', name='apply'),
    url(r'^do_apply$', 'do_apply', name='do_apply'),
    url(r'^msg', 'msg', name='msg'),
    url(r'^aboutus$', 'aboutus', name='aboutus'),
    url(r'^myself$', 'myself', name='myself'),
    url(r'^selfpage$', 'selfpage', name='selfpage'),
    url(r'^fanouts/add$', 'add_fanout', name='add fanout'),
    url(r'^fanouts/do_add$', 'doadd_fanout', name='add fanout'),
    url(r'^fanouts/finish/(.+)$', 'fanout_finish'),
    url(r'^fanouts/items/(.+)$', 'faoutitems'),
    url(r'^fanoutsitems/finish/(.+)$', 'si_finish'),
    url(r'^fanoutsitems/confirm/(.+)$', 'si_confirm'),
    url(r'^fanoutsitems/add$', 'fanoutitemadd'),
    url(r'^wx$', 'indexwx'),
    url(r'^wxdetail/(.+)$', 'detailwx'),
    url(r'^wxfanout$', 'fanoutwx'),
    url(r'^wxfanoutdetail/(.+)$', 'fanoutdetailwx'),
    url(r'^wxprofile$', 'profilewx'),
    url(r'^wxproctx/(.+)/$', 'contentex'),
)
