# -*- coding: UTF-8 -*-
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from collections import OrderedDict
from django.http import HttpResponse,HttpResponseRedirect
from django.db.utils import IntegrityError
import json
import os
import datetime
from website.models import ApplyUser, Fanout, FunkesUser, ScrumItem
from api.wechat import Wechat

fanout_type = {
    'task': '任务',
    'sponsor': '赞助方案',
    'notify': '通知'
}

@csrf_exempt
def index(request):
    return render_to_response('index/index.html', {})

@csrf_exempt
def detail(request, id):
    return render_to_response('index/detail_%s.html' % id, {})

@csrf_exempt
def indexwx(request):
    return render_to_response('index/index_wx.html', {})

@csrf_exempt
def detailwx(request, id):
    return render_to_response('index/detailwx%s.html' % id, {})

@csrf_exempt
def fanoutwx(request):
    fanouts = Fanout.objects.filter(deleted=0)
    fs = []
    for _f in fanouts:
        f = {}
        f['id'] = int(_f.id)
        f['tilte'] = _f.title
        f['url'] = _f.url
        f['desc'] = _f.desc
        f['acceptable'] = _f.acceptable
        f['type'] = fanout_type[_f.type]
        f['finished'] = _f.finished
        f['enddate'] = _f.enddate
        f['createdate'] = datetime.datetime.strftime(_f.created_at, '%Y-%m-%d')
        user_info = _f.userid
        f['user_info'] = user_info
        fs.append(f)
    return render_to_response('index/fanoutwx.html', {'fanouts': fs})

@csrf_exempt
def fanoutdetailwx(request, id):
    fanout = Fanout.objects.get(pk=int(id))
    f = {}
    f['id'] = int(fanout.id)
    f['tilte'] = fanout.title
    f['url'] = fanout.url
    f['desc'] = fanout.desc
    f['acceptable'] = fanout.acceptable
    f['type'] = fanout_type[fanout.type]
    f['finished'] = fanout.finished
    f['enddate'] = fanout.enddate
    f['createdate'] = datetime.datetime.strftime(fanout.created_at, '%Y-%m-%d')
    user_info = fanout.userid
    f['user_info'] = user_info
    
    if request.user.id == fanout.userid.id:
        if fanout.finished == 0:
            f['finish_valid'] = 1

    fitems = ScrumItem.objects.filter(fanoutid=fanout.id)
    _fitems = []
    for _f in fitems:
        i = {}
        i['id'] = _f.id
        i['username'] = _f.userid.name
        i['headimgurl'] = _f.userid.headimgurl
        i['fanout_type'] = fanout_type[_f.fanoutid.type]
        if _f.finished !=0:
            if _f.confirmed != 0:
                i['fanout_finished'] = '确认完成'
            else:
                i['fanout_finished'] = '完成'
        else:
            i['fanout_finished'] = '未完成'
        _fitems.append(i)

    return render_to_response('index/fanoutdetailwx.html', {'fanout': f, 'fitems': _fitems})

@csrf_exempt
def profilewx(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx9e519842ae6dcbcd&redirect_uri=http%3a%2f%2flianzhongsanyuan.com%2fwechat%2flogin&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect')
    fitems = ScrumItem.objects.filter(userid=request.user.id)
    _fitems = []
    for _f in fitems:
        i = {}
        i['id'] = _f.id
        i['username'] = '您'
        i['headimgurl'] = request.user.headimgurl
        i['fanout_type'] = fanout_type[_f.fanoutid.type]
        if _f.finished !=0:
            if _f.confirmed != 0:
                i['fanout_finished'] = '确认完成'
            else:
                i['fanout_finished'] = '完成'
        else:
            i['fanout_finished'] = '未完成'
        _fitems.append(i)
    return render_to_response('index/profilewx.html', {'user': request.user, 'fitems': _fitems})

@csrf_exempt
def contentex(request, id):
    return HttpResponse(json.dumps({}), content_type='application/json;')

@csrf_exempt
def faoutitems(request, id):
    si = ScrumItem.objects.get(pk=int(id))
    fanout = si.fanoutid
    tag = {}
    tag['id'] = int(si.id)
    tag['confirmed'] = si.confirmed
    tag['fid'] = int(fanout.id)
    tag['tilte'] = fanout.title
    tag['url'] = fanout.url
    tag['desc'] = fanout.desc
    tag['acceptable'] = fanout.acceptable
    tag['type'] = fanout_type[fanout.type]
    tag['finished'] = si.finished
    tag['enddate'] = fanout.enddate
    tag['createdate'] = datetime.datetime.strftime(fanout.created_at, '%Y年-%m月-%d日')
    if fanout.userid.id == request.user.id:
        if si.finished and not si.confirmed: 
            tag['confirm_valid'] = 1
 
    return render_to_response('index/fanoutitems.html', {'fanout': tag})

@csrf_exempt
def fanoutitemadd(request):
    _f = request.POST
    fanout = Fanout.objects.get(pk=int(_f['id']))
    user = FunkesUser.objects.get(pk=request.user.id)
    ScrumItem.objects.create(fanoutid=fanout,
                             userid=user)
    return HttpResponse('success', content_type='application/json;')

@csrf_exempt
def signin(request):
    return render_to_response('index/signin.html', {})

@csrf_exempt
def aboutus(request):
    return render_to_response('index/aboutus.html', {})

@csrf_exempt
def apply(request):
    id = request.GET.get('id')
    return render_to_response('index/apply.html', {'id': id})

@csrf_exempt
def do_apply(request):
    data = json.loads(request.body)
    id = request.GET.get('id', '')
    result = 'true'
    try:
        ApplyUser.objects.create(name=data['username'],
                                 phone=data['phone'],
                                 email=data.get('email', ''),
                                 actid=id,
                                 sex=data['sex'],
                                 desc=data.get('desc', ''))
        ApplyUser.save()
    except IntegrityError:
        pass
    except Exception as e:
        pass
    return HttpResponse(result, content_type='application/json;')

@csrf_exempt
def msg(request):
    return render_to_response('index/msg.html', {})

@csrf_exempt
def myself(request):
    return HttpResponseRedirect('/selfpage')

@csrf_exempt
def selfpage(request):
    return render_to_response('index/selfpage.html', {'user': {}})

@csrf_exempt
def add_fanout(request):
    return render_to_response('index/fanoutadd.html')

@csrf_exempt
def doadd_fanout(request):
    fanout = request.POST
    try:
        mfanout = Fanout.objects.create(userid=FunkesUser.objects.get(pk=request.user.id),
                              url=fanout.get('url', ''),
                              title=fanout['topic'],
                              desc=fanout['desc'],
                              acceptable=fanout['acceptable'],
                              type=fanout['type'],
                              enddate=fanout['date'])
    except Exception as e:
        print e.message

    os.mkdir(os.path.abspath('%s/images/fanouts/%s' % (settings.STATICFILES_DIRS[0], str(mfanout.id))))
    file = request.FILES['file']
    with open(os.path.abspath('%s/images/fanouts/%s/%s.png' % (settings.STATICFILES_DIRS[0], str(mfanout.id), str(mfanout.id))), 'wb+') as w:
         for f in file.chunks():
             w.write(f)

    return HttpResponse('success')

@csrf_exempt
def fanout_finish(request, id):
    fanout = Fanout.objects.get(pk=int(id))
    fanout.finished = 1
    fanout.save()
    return HttpResponse('success')

@csrf_exempt
def si_finish(request, id):
    si = ScrumItem.objects.get(pk=int(id))
    si.finished = 1
    si.save()
    return HttpResponse('success')

@csrf_exempt
def si_confirm(request, id):
    si = ScrumItem.objects.get(pk=int(id))
    si.confirmed = 1
    si.save()
    return HttpResponse('success')
