# -*- coding: UTF-8 -*-
from django.conf.urls import url
from django.conf.urls import patterns

urlpatterns = patterns(
    'website.store.views',
    url(r'^$', 'index', name='index'),
    url(r'^detail/(?P<pk>\d+)$', 'detail', name='detail'),
    url(r'^shoppingcart/(?P<pk>\d+)$$', 'shopping_cart', name='shopping cart'),
    url(r'^shopcart$', 'shop_cart', name='shopping cart'),
    url(r'^shopcart/add', 'do_add_cert', name='cart add'),
    url(r'^calculate$', 'calculate', name='calculate'),
    url(r'^order$', 'order', name='order'),
    url(r'^order/add$', 'do_order', name='do order'),
    url(r'^order/cal$', 'cancel_order', name='cancel order'),
    url(r'^test$', 'test', name='test'),
    url(r'^testmail$', 'testmail'),
)
