# -*- coding: UTF-8 -*-
from django.conf.urls import url
from django.conf.urls import patterns

urlpatterns = patterns(
    'website.mobile.views',
    url(r'^$', 'index', name='index'),
    url(r'^store/index$', 'store_index'),
    url(r'^store/detail/(?P<pk>\d+)$', 'store_detail'),
    url(r'^store/cart$', 'store_cart'),
    url(r'^store/cart/add$', 'add_store_cart'),
    url(r'^order/index$', 'order_index'),
    url(r'^order/add$', 'order_add'),
    url(r'^order/add/confirm$', 'order_confirm'),
    url(r'^order/cancel$', 'order_cancel'),
    url(r'^order/cancel/confirm$', 'order_cancel_confirm'),
    url(r'^cart/index$', 'cart_index'),
    url(r'^article/index$', 'article_index'),
    url(r'^profile/index$', 'profile_index'),
    url(r'^profile/user/edit$', 'profile_user_edit'),
    url(r'^store/add$', 'sample_add'),
    url(r'^store/do_add$', 'do_sample_add'),
    url(r'^article/detail/(.+)/$', 'article_detail'),
)
