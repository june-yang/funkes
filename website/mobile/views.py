# -*- coding: UTF-8 -*-
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from collections import OrderedDict
from django.http import HttpResponse,HttpResponseRedirect
from django.db.utils import IntegrityError
import json
import os
import datetime
from website.models import ApplyUser, Fanout, FunkesUser, ScrumItem, Sample, ShopCart, Order, OrderSampleMaper, ShopCart
from website.index.mail import MailText, MailContent, Mail
from django.template.loader import render_to_string

@csrf_exempt
def index(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx9e519842ae6dcbcd&redirect_uri=http%3a%2f%2flianzhongsanyuan.com%2fwechat%2flogin&response_type=code&scope=snsapi_userinfo&state=2#wechat_redirect')

    return render_to_response('mobile/index.html', {})


@csrf_exempt
def store_index(request):
    sm = []
    q = request.GET.get('q', None)
    if q:
        _sm = Sample.objects.filter(type=q, deleted=0)
    else:
        _sm = Sample.objects.filter(deleted=0)
    for s in _sm:
        sm.append({
            'id': s.id,
            'title': s.title,
            'image': json.loads(s.header_images)[0],
            'price': s.price
        })
    return render_to_response('mobile/store_index.html', {'sm': sm})

@csrf_exempt
def store_detail(request, pk):
    sm = Sample.objects.get(pk=int(pk))
    _sm = {
        'id': sm.id,
        'title': sm.title,
        'price': sm.price,
        'header_image': json.loads(sm.header_images)[0],
        'detail_images': [i for i in json.loads(sm.detail_images)],
        'remian': sm.remain,
        'description': sm.desc
    }
    return render_to_response('mobile/store_detail.html', {'sm': _sm})

@csrf_exempt
def store_cart(request):
    id = request.GET.get('id')
    sm = Sample.objects.get(pk=int(id))

    _sm = {
        'id': sm.id,
        'title': sm.title,
        'price': sm.price,
        'require': json.loads(sm.require),
        'header_image': json.loads(sm.header_images)[0],
        'detail_images': [i for i in json.loads(sm.detail_images)],
        'remian': sm.remain,
        'description': sm.desc
    }
    return render_to_response('mobile/store_cart.html', {'sm': _sm})

@csrf_exempt
def add_store_cart(request):
    cert = json.loads(request.body)
    sc = ShopCart.objects.create(
        sample_id=Sample.objects.get(pk=int(cert['sample_id'])),
        count=int(cert['count']),
        price=float(cert['price']),
        extra=json.dumps(cert['sku_select']),
        userid=FunkesUser.objects.get(pk=request.user.id)
    )
    sc.save()
    return HttpResponse('success', content_type='application/json;')

@csrf_exempt
def article_index(request):
    return render_to_response('mobile/article_index.html', {})

@csrf_exempt
def profile_index(request):
    return render_to_response('mobile/profile_index.html', {'user': request.user})

@csrf_exempt
def profile_user_edit(request):
    return render_to_response('mobile/profile_edit.html', {'user': request.user})

@csrf_exempt
def cart_index(request):
    scs = ShopCart.objects.filter(deleted=0, userid=request.user.id)
    _scs = []
    for s in scs:
        _scs.append({
            'id': s.id,
            'sample_id': s.sample_id.id,
            'title': s.sample_id.title,
            'image': json.loads(s.sample_id.header_images)[0],
            'price': s.price,
            'count': s.count
        })
    require_phone_and_email = 'false'
    if request.user.phone and request.user.email:
        require_phone_and_email = 'true'
    return render_to_response('mobile/cart_index.html', {'scs': _scs, 'require_phone_and_email': require_phone_and_email})

@csrf_exempt
def order_index(request):
    order = Order.objects.filter(status=0, userid=request.user.id)
    os = []
    for o in order:
        om = OrderSampleMaper.objects.filter(order_id=o.id)
        price = 0.00
        for _o in om:
            price += _o.price*_o.count

        os.append({
            'id': o.id,
            'sample_count': len(om),
            'price': price - o.disCount,
            'status': o.status,
            'images': [json.loads(s.sample_id.header_images)[0] for s in om]
        })
    return render_to_response('mobile/order_index.html', {'os': os})

@csrf_exempt
def order_cancel_confirm(request):
    return render_to_response('mobile/order_cancel.html', {})

@csrf_exempt
def order_cancel(request):
    calinfo = json.loads(request.body)
    mt = [MailText(u'来自%s(电话:%s,email:%s)的订单取消请求' % (request.user.name, request.user.phone, request.user.email))]
    for id in calinfo['ids']:
        order = Order.objects.get(pk=int(id))
        order.status = 10
        order.save()
        mt.append(MailText(u'uuid: %s' % order.uuid))
    mc = MailContent('以下是详细信息:', mt)
    email = Mail(u'取消订单提醒', ['371002491@qq.com'], ['371002491@qq.com'], mc)
    email.send()
    return HttpResponse('success')

@csrf_exempt
def order_add(request):
    certinfo = json.loads(request.body)
    order = Order.objects.create(
        desc=certinfo.get('extra', ''),
        userid=FunkesUser.objects.get(pk=request.user.id),
    )
    order.save()
    orders = []
    for id in certinfo['ids']:
        sc = ShopCart.objects.get(pk=int(id))
        osm = OrderSampleMaper.objects.create(
            sample_id=sc.sample_id,
            order_id=order,
            userid=FunkesUser.objects.get(pk=request.user.id),
            count=sc.count,
            price=sc.price,
            extra=sc.extra
        )
        osm.save()
        sc.deleted = 1
        sc.save()

        # parser require info
        req = u''
        sm_req_info = json.loads(sc.sample_id.require)
        cert_select = json.loads(sc.extra)
        for cs in cert_select:
            item = [s for s in sm_req_info if s['id'] == cs['id']][0]
            selected_item = [s for s in item['values'] if s['id'] == cs['select']][0]
            req += u'%s: %s;' % (item['title'], selected_item['name'])
        # orders.append({'count': sc.count, 'title': sc.sample_id.title, 'info': req, 'url': 'http://www.lianzhongsanyuan.com/store/detail/%s' % str(sc.sample_id.id)})
        orders.append({'count': sc.count, 'title': sc.sample_id.title, 'info': req})

    mt = [MailText(u'来自%s(电话:%s,email:%s)的订单请求' % (request.user.name, request.user.phone, request.user.email))]
    mc = MailContent('以下是详细信息:', mt)
    mc.extra = render_to_string('store/_order_detail.html', {'orders': orders})
    email = Mail(u'订单提醒', ['371002491@qq.com'], ['371002491@qq.com'], mc)
    email.send()
    return HttpResponse('success', content_type='application/json;')

@csrf_exempt
def order_confirm(request):
    price = request.GET.get('price')
    return render_to_response('mobile/order_add.html', {'price': price})

@csrf_exempt
def sample_add(request):
    return render_to_response('mobile/store_add.html', {})

@csrf_exempt
def do_sample_add(request):
    sap = request.POST
    images_count = int(sap['image_count'])
    images = []
    for i in range(1, images_count + 1):
        images.append(request.FILES['images-%s' % i])

    i = 1
    require = []

    if sap.get('size', None):
        size = json.loads(sap['size'])
        size_obj = {
            'title': u'尺码',
            'id': i,
            'values': size
        }
        require.append(size_obj)
        i += 1

    if sap.get('color', None):
        color = json.loads(sap['color'])
        color_obj = {
            'title': u'颜色',
            'id': i,
            'values': color
        }
        require.append(color_obj)
        i += 1

    require.append({
                    "title": u"租期",
                    "id": i,
                     "values":
                         [
                             {
                                 "id": 1,
                                 "name": "一天",
                                 "price": 0,
                                 "default": True
                             },
                             {
                                 "id": 2,
                                 "name": "两天",
                                 "price": float(sap['price'])
                             },
                             {
                                 "id": 3,
                                 "name" :"三天",
                                 "price": float(sap['price']) * 2
                             },
                             {
                                 "id": 4,
                                 "name" :"多于三天",
                                 "price": 0
                             }
                         ]
                     })

    smp = Sample.objects.create(title=sap['title'],
                                 desc=sap['desc'],
                                 type=sap['type'],
                                 price=float(sap['price']),
                                 remain=50,
                                 keywords=u'',
                                 require=json.dumps(require)
                                 )
    smp.save()
    os.mkdir(os.path.abspath('%s/images/store/%s' % (settings.STATICFILES_DIRS[0], str(smp.id))))
    with open(os.path.abspath('%s/images/store/%s/1.png' % (settings.STATICFILES_DIRS[0], str(smp.id))), 'wb+') as w:
        for f in images[0].chunks():
            w.write(f)
        smp.header_images = json.dumps(['/static/images/store/%s/1.png?v=1' % str(smp.id)])

    i = 0
    detail_images = []
    os.mkdir(os.path.abspath('%s/images/store/%s/details' % (settings.STATICFILES_DIRS[0], str(smp.id))))
    for image in images:
        i += 1
        detail_images.append('/static/images/store/%s/details/%s.png?v=1' % (str(smp.id), str(i)))
        with open(os.path.abspath('%s/images/store/%s/details/%s.png' % (settings.STATICFILES_DIRS[0], str(smp.id), str(i))), 'wb+') as w:
            for f in image.chunks():
                w.write(f)
    smp.detail_images = json.dumps(detail_images)
    smp.save()
    return HttpResponse('success', content_type='application/json;')

@csrf_exempt
def detail(request, pk):
    sm = Sample.objects.get(pk=int(pk))
    _sm = {
        'id': sm.id,
        'title': sm.title,
        'price': sm.price,
        'header_image': json.loads(sm.header_images)[0],
        'detail_images': [i for i in json.loads(sm.detail_images)],
        'remian': sm.remain,
        'description': sm.desc
    }
    return render_to_response('store/store_items.html', {'sm': _sm})

@csrf_exempt
def shopping_cart(request, pk):
    sm = Sample.objects.get(pk=int(pk))

    _sm = {
        'id': sm.id,
        'title': sm.title,
        'price': sm.price,
        'require': json.loads(sm.require),
        'header_image': json.loads(sm.header_images)[0],
        'detail_images': [i for i in json.loads(sm.detail_images)],
        'remian': sm.remain,
        'description': sm.desc
    }
    return render_to_response('store/shopping-cart.html', {'sm': _sm})


@csrf_exempt
def do_add_cert(request):
    cert = json.loads(request.body)
    sc = ShopCart.objects.create(
        sample_id=Sample.objects.get(pk=int(cert['sample_id'])),
        count=int(cert['count']),
        price=float(cert['price']),
        extra=json.dumps(cert['sku_select']),
        userid=FunkesUser.objects.get(pk=request.user.id)
    )
    sc.save()
    return HttpResponse('success', content_type='application/json;')

@csrf_exempt
def shop_cart(request):
    scs = ShopCart.objects.filter(deleted=0, userid=request.user.id)
    _scs = []
    for s in scs:
        _scs.append({
            'id': s.id,
            'sample_id': s.sample_id.id,
            'title': s.sample_id.title,
            'image': json.loads(s.sample_id.header_images)[0],
            'price': s.price,
            'count': s.count
        })
    require_phone_and_email = 'false'
    if request.user.phone and request.user.email:
        require_phone_and_email = 'true'

    return render_to_response('store/shopcart.html', {'scs': _scs, 'require_phone_and_email': require_phone_and_email})

@csrf_exempt
def calculate(request):
    return HttpResponse('success', content_type='application/json;')

@csrf_exempt
def order(request):
    order = Order.objects.filter(status=0, userid=request.user.id)
    os = []
    for o in order:
        om = OrderSampleMaper.objects.filter(order_id=o.id)
        price = 0.00
        for _o in om:
            price += _o.price*_o.count

        os.append({
            'id': o.id,
            'sample_count': len(om),
            'price': price - o.disCount,
            'status': o.status
        })
    return render_to_response('store/order.html', {'os': os})

@csrf_exempt
def cancel_order(request):
    calinfo = json.loads(request.body)
    mt = [MailText(u'来自%s(电话:%s,email:%s)的订单取消请求' % (request.user.name, request.user.phone, request.user.email))]
    for id in calinfo['ids']:
        order = Order.objects.get(pk=int(id))
        order.status = 10
        order.save()
        mt.append(MailText(u'uuid: %s' % order.uuid))
    mc = MailContent('以下是详细信息:', mt)
    email = Mail(u'取消订单提醒', ['371002491@qq.com'], ['371002491@qq.com'], mc)
    email.send()
    return HttpResponse('success')


@csrf_exempt
def do_order(request):
    certinfo = json.loads(request.body)
    order = Order.objects.create(
        desc=certinfo.get('extra', ''),
        userid=FunkesUser.objects.get(pk=request.user.id),
    )
    order.save()
    orders = []
    for id in certinfo['ids']:
        sc = ShopCart.objects.get(pk=int(id))
        osm = OrderSampleMaper.objects.create(
            sample_id=sc.sample_id,
            order_id=order,
            userid=FunkesUser.objects.get(pk=request.user.id),
            count=sc.count,
            price=sc.price,
            extra=sc.extra
        )
        osm.save()
        sc.deleted = 1
        sc.save()

        # parser require info
        req = u''
        sm_req_info = json.loads(sc.sample_id.require)
        cert_select = json.loads(sc.extra)
        for cs in cert_select:
            item = [s for s in sm_req_info if s['id'] == cs['id']][0]
            selected_item = [s for s in item['values'] if s['id'] == cs['select']][0]
            req += u'%s: %s;' % (item['title'], selected_item['name'])
        # orders.append({'count': sc.count, 'title': sc.sample_id.title, 'info': req, 'url': 'http://www.lianzhongsanyuan.com/store/detail/%s' % str(sc.sample_id.id)})
        orders.append({'count': sc.count, 'title': sc.sample_id.title, 'info': req})

    mt = [MailText(u'来自%s(电话:%s,email:%s)的订单请求' % (request.user.name, request.user.phone, request.user.email))]
    mc = MailContent('以下是详细信息:', mt)
    mc.extra = render_to_string('store/_order_detail.html', {'orders': orders})
    email = Mail(u'订单提醒', ['371002491@qq.com'], ['371002491@qq.com'], mc)
    email.send()
    return HttpResponse('success', content_type='application/json;')

@csrf_exempt
def article_detail(request, id):
    return render_to_response('mobile/detail_%s.html' % id, {})

@csrf_exempt
def test(request):
    smp1 = Sample.objects.create(title=u'2016新款藏族舞蹈演出服儿童成人藏族水袖藏服蒙古民族表演服装女',
                                desc=u'2016新款藏族舞蹈演出服儿童成人藏族水袖藏服蒙古民族表演服装女',
                                require=json.dumps(
                                    [
                                        {
                                            "title": u"租期",
                                            "id": 1,
                                             "values":
                                                 [
                                                     {
                                                         "id": 1,
                                                         "name": "一天",
                                                         "price": 0,
                                                         "default": True
                                                     },
                                                     {
                                                         "id": 2,
                                                         "name": "两天",
                                                         "price": 50
                                                     },
                                                     {
                                                         "id": 3,
                                                         "name" :"三天",
                                                         "price": 100
                                                     },
                                                     {
                                                         "id": 4,
                                                         "name" :"多于三天",
                                                         "price": 0
                                                     }
                                                 ]
                                             }
                                    ]),
                                header_images=json.dumps(
                                    ['/static/images/store/1/1.jpg?v=1']
                                ),
                                detail_images=json.dumps(
                                    [
                                        '/static/images/store/1/details/1.jpg?v=1',
                                        '/static/images/store/1/details/2.jpg?v=1',
                                        '/static/images/store/1/details/3.png?v=1'
                                    ]
                                ),
                                type='1',
                                keywords='',
                                price=50,
                                remain=100
                                )
    smp1.save()
    smp2 = Sample.objects.create(title=u'2016新款汉服女装曲裾美人心计古装仙女襦裙表演出服毕业成人礼服',
                                desc=u'2016新款汉服女装曲裾美人心计古装仙女襦裙表演出服毕业成人礼服',
                                require=json.dumps(
                                    [
                                        {
                                            "title": u"租期",
                                            "id": 1,
                                             "values":
                                                 [
                                                     {
                                                         "id": 1,
                                                         "name": "一天",
                                                         "price": 0,
                                                         "default": True
                                                     },
                                                     {
                                                         "id": 2,
                                                         "name": "两天",
                                                         "price": 50
                                                     },
                                                     {
                                                         "id": 3,
                                                         "name" :"三天",
                                                         "price": 100
                                                     },
                                                     {
                                                         "id": 4,
                                                         "name" :"多于三天",
                                                         "price": 0
                                                     }
                                                 ]
                                             }
                                    ]),
                                header_images=json.dumps(
                                    ['/static/images/store/2/1.jpg?v=1']
                                ),
                                detail_images=json.dumps(
                                    [
                                        '/static/images/store/2/details/1.jpg?v=1',
                                    ]
                                ),
                                type='1',
                                keywords='',
                                price=50,
                                remain=100
                                )
    smp2.save()
    smp3 = Sample.objects.create(title=u'2016新款汉服女装曲裾美人心计古装仙女襦裙表演出服毕业成人礼服',
                                desc=u'2016新款汉服女装曲裾美人心计古装仙女襦裙表演出服毕业成人礼服',
                                require=json.dumps(
                                    [
                                        {
                                            "title": u"租期",
                                            "id": 1,
                                             "values":
                                                 [
                                                     {
                                                         "id": 1,
                                                         "name": "一天",
                                                         "price": 0,
                                                         "default": True
                                                     },
                                                     {
                                                         "id": 2,
                                                         "name": "两天",
                                                         "price": 50
                                                     },
                                                     {
                                                         "id": 3,
                                                         "name" :"三天",
                                                         "price": 100
                                                     },
                                                     {
                                                         "id": 4,
                                                         "name" :"多于三天",
                                                         "price": 0
                                                     }
                                                 ]
                                             }
                                    ]),
                                header_images=json.dumps(
                                    ['/static/images/store/3/1.jpg?v=1']
                                ),
                                detail_images=json.dumps(
                                    [
                                        '/static/images/store/3/details/1.jpg?v=1',
                                    ]
                                ),
                                type='1',
                                keywords='',
                                price=50,
                                remain=100
                                )
    smp3.save()
    smp4 = Sample.objects.create(title=u'2016新款汉服女装曲裾美人心计古装仙女襦裙表演出服毕业成人礼服',
                                desc=u'2016新款汉服女装曲裾美人心计古装仙女襦裙表演出服毕业成人礼服',
                                require=json.dumps(
                                    [
                                        {
                                            "title": u"租期",
                                            "id": 1,
                                             "values":
                                                 [
                                                     {
                                                         "id": 1,
                                                         "name": "一天",
                                                         "price": 0,
                                                         "default": True
                                                     },
                                                     {
                                                         "id": 2,
                                                         "name": "两天",
                                                         "price": 50
                                                     },
                                                     {
                                                         "id": 3,
                                                         "name" :"三天",
                                                         "price": 100
                                                     },
                                                     {
                                                         "id": 4,
                                                         "name" :"多于三天",
                                                         "price": 0
                                                     }
                                                 ]
                                             }
                                    ]),
                                header_images=json.dumps(
                                    ['/static/images/store/4/1.jpg?v=1']
                                ),
                                detail_images=json.dumps(
                                    [
                                        '/static/images/store/4/details/1.jpg?v=1',
                                    ]
                                ),
                                type='1',
                                keywords='',
                                price=50,
                                remain=100
                                )
    smp4.save()
    smp5 = Sample.objects.create(title=u'2016新款汉服女装曲裾美人心计古装仙女襦裙表演出服毕业成人礼服',
                                desc=u'2016新款汉服女装曲裾美人心计古装仙女襦裙表演出服毕业成人礼服',
                                require=json.dumps(
                                    [
                                        {
                                            "title": u"租期",
                                            "id": 1,
                                             "values":
                                                 [
                                                     {
                                                         "id": 1,
                                                         "name": "一天",
                                                         "price": 0,
                                                         "default": True
                                                     },
                                                     {
                                                         "id": 2,
                                                         "name": "两天",
                                                         "price": 50
                                                     },
                                                     {
                                                         "id": 3,
                                                         "name" :"三天",
                                                         "price": 100
                                                     },
                                                     {
                                                         "id": 4,
                                                         "name" :"多于三天",
                                                         "price": 0
                                                     }
                                                 ]
                                             }
                                    ]),
                                header_images=json.dumps(
                                    ['/static/images/store/5/1.jpg?v=1']
                                ),
                                detail_images=json.dumps(
                                    [
                                        '/static/images/store/5/details/1.jpg?v=1',
                                    ]
                                ),
                                type='1',
                                keywords='',
                                price=50,
                                remain=100
                                )
    smp5.save()
    smp6 = Sample.objects.create(title=u'2016新款汉服女装曲裾美人心计古装仙女襦裙表演出服毕业成人礼服',
                                desc=u'2016新款汉服女装曲裾美人心计古装仙女襦裙表演出服毕业成人礼服',
                                require=json.dumps(
                                    [
                                        {
                                            "title": u"租期",
                                            "id": 1,
                                             "values":
                                                 [
                                                     {
                                                         "id": 1,
                                                         "name": "一天",
                                                         "price": 0,
                                                         "default": True
                                                     },
                                                     {
                                                         "id": 2,
                                                         "name": "两天",
                                                         "price": 50
                                                     },
                                                     {
                                                         "id": 3,
                                                         "name" :"三天",
                                                         "price": 100
                                                     },
                                                     {
                                                         "id": 4,
                                                         "name" :"多于三天",
                                                         "price": 0
                                                     }
                                                 ]
                                             }
                                    ]),
                                header_images=json.dumps(
                                    ['/static/images/store/6/1.jpg?v=1']
                                ),
                                detail_images=json.dumps(
                                    [
                                        '/static/images/store/6/details/1.jpg?v=1',
                                    ]
                                ),
                                type='1',
                                keywords='',
                                price=50,
                                remain=100
                                )
    smp6.save()
    smp7 = Sample.objects.create(title=u'DS女成人爵士舞服装现代舞练舞服嘻哈亮片舞蹈服',
                                desc=u'DS女成人爵士舞服装现代舞练舞服嘻哈亮片舞蹈服',
                                require=json.dumps(
                                    [
                                        {
                                            "title": u"租期",
                                            "id": 1,
                                             "values":
                                                 [
                                                     {
                                                         "id": 1,
                                                         "name": "一天",
                                                         "price": 0,
                                                         "default": True
                                                     },
                                                     {
                                                         "id": 2,
                                                         "name": "两天",
                                                         "price": 40
                                                     },
                                                     {
                                                         "id": 3,
                                                         "name" :"三天",
                                                         "price": 80
                                                     },
                                                     {
                                                         "id": 4,
                                                         "name" :"多于三天",
                                                         "price": 0
                                                     }
                                                 ]
                                             }
                                    ]),
                                header_images=json.dumps(
                                    ['/static/images/store/7/1.jpg?v=1']
                                ),
                                detail_images=json.dumps(
                                    [
                                        '/static/images/store/7/details/1.jpg?v=1',
                                    ]
                                ),
                                type='1',
                                keywords='',
                                price=40,
                                remain=100
                                )
    smp7.save()
    smp8 = Sample.objects.create(title=u'成人白雪公主裙舞台演出cosplay服装',
                                desc=u'成人白雪公主裙舞台演出cosplay服装',
                                require=json.dumps(
                                    [
                                        {
                                            "title": u"租期",
                                            "id": 1,
                                             "values":
                                                 [
                                                     {
                                                         "id": 1,
                                                         "name": "一天",
                                                         "price": 0,
                                                         "default": True
                                                     },
                                                     {
                                                         "id": 2,
                                                         "name": "两天",
                                                         "price": 60
                                                     },
                                                     {
                                                         "id": 3,
                                                         "name" :"三天",
                                                         "price": 120
                                                     },
                                                     {
                                                         "id": 4,
                                                         "name" :"多于三天",
                                                         "price": 0
                                                     }
                                                 ]
                                             }
                                    ]),
                                header_images=json.dumps(
                                    ['/static/images/store/8/1.jpg?v=1']
                                ),
                                detail_images=json.dumps(
                                    [
                                        '/static/images/store/8/details/1.jpg?v=1',
                                    ]
                                ),
                                type='1',
                                keywords='',
                                price=60,
                                remain=100
                                )
    smp8.save()
    smp9 = Sample.objects.create(title=u'成人儿童小八路军演出服红军服抗战服装红卫兵服表演衣服男女军装',
                                desc=u'成人儿童小八路军演出服红军服抗战服装红卫兵服表演衣服男女军装',
                                require=json.dumps(
                                    [
                                        {
                                            "title": u"租期",
                                            "id": 1,
                                             "values":
                                                 [
                                                     {
                                                         "id": 1,
                                                         "name": "一天",
                                                         "price": 0,
                                                         "default": True
                                                     },
                                                     {
                                                         "id": 2,
                                                         "name": "两天",
                                                         "price": 40
                                                     },
                                                     {
                                                         "id": 3,
                                                         "name" :"三天",
                                                         "price": 80
                                                     },
                                                     {
                                                         "id": 4,
                                                         "name" :"多于三天",
                                                         "price": 0
                                                     }
                                                 ]
                                             }
                                    ]),
                                header_images=json.dumps(
                                    ['/static/images/store/9/1.jpg?v=1']
                                ),
                                detail_images=json.dumps(
                                    [
                                        '/static/images/store/9/details/1.jpg?v=1',
                                        '/static/images/store/9/details/2.jpg?v=1',
                                        '/static/images/store/9/details/3.jpg?v=1',
                                        '/static/images/store/9/details/4.jpg?v=1',
                                        '/static/images/store/9/details/5.jpg?v=1',
                                        '/static/images/store/9/details/6.jpg?v=1',
                                        '/static/images/store/9/details/7.jpg?v=1',
                                    ]
                                ),
                                type='1',
                                keywords='',
                                price=40,
                                remain=100
                                )
    smp9.save()
    smp10 = Sample.objects.create(title=u'复古中式古装演出服民国秀禾服影楼摄影新娘敬酒服媒婆服装表演服',
                                desc=u'复古中式古装演出服民国秀禾服影楼摄影新娘敬酒服媒婆服装表演服',
                                require=json.dumps(
                                    [
                                        {
                                            "title": u"租期",
                                            "id": 1,
                                             "values":
                                                 [
                                                     {
                                                         "id": 1,
                                                         "name": "一天",
                                                         "price": 0,
                                                         "default": True
                                                     },
                                                     {
                                                         "id": 2,
                                                         "name": "两天",
                                                         "price": 80
                                                     },
                                                     {
                                                         "id": 3,
                                                         "name" :"三天",
                                                         "price": 160
                                                     },
                                                     {
                                                         "id": 4,
                                                         "name" :"多于三天",
                                                         "price": 0
                                                     }
                                                 ]
                                             }
                                    ]),
                                header_images=json.dumps(
                                    ['/static/images/store/10/1.jpg?v=1']
                                ),
                                detail_images=json.dumps(
                                    [
                                        '/static/images/store/10/details/1.jpg?v=1',
                                        '/static/images/store/10/details/2.jpg?v=1',
                                    ]
                                ),
                                type='1',
                                keywords='',
                                price=80,
                                remain=100
                                )
    smp10.save()
    smp11 = Sample.objects.create(title=u'古装服装仙女古筝演出服古代唐朝公主贵妃襦裙舞蹈汉服影楼写真服',
                                desc=u'古装服装仙女古筝演出服古代唐朝公主贵妃襦裙舞蹈汉服影楼写真服',
                                require=json.dumps(
                                    [
                                        {
                                            "title": u"租期",
                                            "id": 1,
                                             "values":
                                                 [
                                                     {
                                                         "id": 1,
                                                         "name": "一天",
                                                         "price": 0,
                                                         "default": True
                                                     },
                                                     {
                                                         "id": 2,
                                                         "name": "两天",
                                                         "price": 80
                                                     },
                                                     {
                                                         "id": 3,
                                                         "name" :"三天",
                                                         "price": 160
                                                     },
                                                     {
                                                         "id": 4,
                                                         "name" :"多于三天",
                                                         "price": 0
                                                     }
                                                 ]
                                             }
                                    ]),
                                header_images=json.dumps(
                                    ['/static/images/store/11/1.jpg?v=1']
                                ),
                                detail_images=json.dumps(
                                    [
                                        '/static/images/store/11/details/1.jpg?v=1',
                                        '/static/images/store/11/details/2.jpg?v=1',
                                    ]
                                ),
                                type='1',
                                keywords='',
                                price=80,
                                remain=100
                                )
    smp11.save()
    return HttpResponse('success')

@csrf_exempt
def testmail(request):
    mt = [MailText('这是测试第一行'), MailText('这是测试第一行'), MailText('这是测试第一行')]
    mc = MailContent('这是一个测试邮件!', mt)
    mc.extra = render_to_string('store/_order_detail.html', {'orders': [
        {'name': '1', 'url': 'sdsd'}, {'name': '1', 'url': 'ssdsd'}
    ]})
    email = Mail('测试标题', ['371002491@qq.com'], ['371002491@qq.com'], mc)
    email.send()
    return HttpResponse('success')


