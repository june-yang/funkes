from django.db import models
from django.utils import timezone
from website.user import User
import uuid

class FunkesUser(models.Model):
    id = models.AutoField(primary_key=True)
    weid = models.CharField(max_length=100, verbose_name=u"wechatid", default='')
    name = models.CharField(default=None, max_length=50, verbose_name=u"username")
    sex = models.CharField(default=None, max_length=50, verbose_name=u"sex")
    phone = models.CharField(default=None, max_length=128, verbose_name=u"phone", null=True)
    email = models.CharField(default=None, max_length=128, verbose_name=u"email", null=True)
    headimgurl = models.CharField(default=None, max_length=600, verbose_name=u"headimgurl", null=True)
    desc = models.CharField(default=None, max_length=400, verbose_name=u"description", null=True)
    deleted = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=timezone.now, verbose_name=u"created_at")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name=u"updated_at")

class ApplyUser(models.Model):
    id = models.AutoField(primary_key=True)
    userid = models.ForeignKey('FunkesUser')
    weid = models.CharField(max_length=100, verbose_name=u"wechatid", default='')
    actid = models.CharField(max_length=100, verbose_name=u"activateid", default='')
    name = models.CharField(default=None, max_length=50, verbose_name=u"username", null=True)
    sex = models.CharField(default=None, max_length=50, verbose_name=u"sex", null=True)
    phone = models.CharField(default=None, max_length=128, verbose_name=u"phone", null=True)
    email = models.CharField(default=None, max_length=128, verbose_name=u"email", null=True)
    desc = models.CharField(default=None, max_length=400, verbose_name=u"description", null=True)
    created_at = models.DateTimeField(default=timezone.now, verbose_name=u"created_at")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name=u"updated_at")

class Fanout(models.Model):
    id = models.AutoField(primary_key=True)
    userid = models.ForeignKey('FunkesUser')
    url = models.CharField(max_length=500, verbose_name=u"url", default='', null=True)
    title = models.CharField(default=None, max_length=50, verbose_name=u"title", null=True)
    desc = models.CharField(default=None, max_length=400, verbose_name=u"description", null=True)
    acceptable = models.IntegerField(default=0)
    type = models.CharField(default=None, max_length=50, verbose_name=u"type")
    finished = models.IntegerField(default=0)
    enddate = models.DateTimeField(default=timezone.now, verbose_name=u"enddate")
    deleted = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=timezone.now, verbose_name=u"created_at")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name=u"updated_at")

class ScrumItem(models.Model):
    id = models.AutoField(primary_key=True)
    fanoutid = models.ForeignKey('Fanout')
    userid = models.ForeignKey('FunkesUser')
    finished = models.IntegerField(default=0)
    confirmed = models.IntegerField(default=0)
    mediaurl = models.CharField(max_length=500, verbose_name=u"url", default='', null=True)
    deleted = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=timezone.now, verbose_name=u"created_at")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name=u"updated_at")

class Article(models.Model):
    id = models.AutoField(primary_key=True)
    fanoutid = models.ForeignKey('Fanout')
    userid = models.ForeignKey('FunkesUser')
    title = models.CharField(max_length=50, verbose_name=u"title", null=True)
    text = models.TextField(max_length=4000, verbose_name=u"text", null=True)
    desc = models.CharField(max_length=400, verbose_name=u"description", null=True)
    likes = models.IntegerField(default=0)
    browse = models.IntegerField(default=0)
    deleted = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=timezone.now, verbose_name=u"created_at")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name=u"updated_at")

class Sample(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=50, verbose_name=u"title", null=True)
    desc = models.CharField(max_length=400, verbose_name=u"description", null=True)
    browse = models.IntegerField(default=0)
    require = models.CharField(max_length=5000, verbose_name=u"description", null=True)
    header_images = models.CharField(max_length=500, verbose_name=u"description", null=True)
    detail_images = models.CharField(max_length=1000, verbose_name=u"description", null=True)
    type = models.CharField(max_length=100, verbose_name=u"type", null=True)
    keywords = models.CharField(max_length=200, verbose_name=u"keywords", null=True)
    price = models.FloatField(default=0.00, verbose_name=u'price')
    remain = models.IntegerField(default=0, verbose_name=u'remain')

    deleted = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=timezone.now, verbose_name=u"created_at")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name=u"updated_at")

class Order(models.Model):
    id = models.AutoField(primary_key=True)
    uuid = models.UUIDField(verbose_name=u"uuid", default=uuid.uuid4)
    desc = models.CharField(max_length=400, verbose_name=u"description", null=True)
    userid = models.ForeignKey('FunkesUser')
    # totolSettle = models.FloatField(default=0.00, verbose_name=u"totol settle")
    disCount = models.FloatField(default=0.00, verbose_name=u"discount")

    status = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=timezone.now, verbose_name=u"created_at")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name=u"updated_at")

class OrderSampleMaper(models.Model):
    id = models.AutoField(primary_key=True)
    sample_id = models.ForeignKey('Sample')
    order_id = models.ForeignKey('Order')
    userid = models.ForeignKey('FunkesUser')
    count = models.IntegerField(default=1, verbose_name='count')
    price = models.FloatField(default=0.00, verbose_name=u"price")
    extra = models.CharField(max_length=400, verbose_name=u"extra", null=True)

    deleted = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=timezone.now, verbose_name=u"created_at")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name=u"updated_at")

class ShopCart(models.Model):
    id = models.AutoField(primary_key=True)
    count = models.IntegerField(default=1, verbose_name='count')
    price = models.FloatField(default=0.00, verbose_name=u"totol price")
    extra = models.CharField(max_length=400, verbose_name=u"extra", null=True)
    sample_id = models.ForeignKey('Sample')
    userid = models.ForeignKey('FunkesUser')

    deleted = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=timezone.now, verbose_name=u"created_at")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name=u"updated_at")
