# -*- coding: UTF-8 -*-

from django.contrib.auth import models
from django.db import models as db_models
from django.utils.translation import ugettext_lazy as _

class User(models.AbstractBaseUser, models.AnonymousUser):
    funkes_user_id = db_models.CharField(primary_key=True, max_length=255, default=None)
    USERNAME_FIELD = 'funkes_user_id'
    REQUIRED_FIELDS = []

    def __init__(self, id=None,
                 openid=None,
                 name=None,
                 unionid=None,
                 phone=None,
                 email=None,
                 sex=None,
                 province=None,
                 city=None,
                 country=None,
                 headimgurl=None):
        self.id = id
        self.funkes_user_id = id
        self.openid = openid
        self.unionid = unionid
        self.phone = phone
        self.email = email
        self.name = name
        self.sex = sex
        self.province = province
        self.city = city
        self.country = country
        self.headimgurl = headimgurl

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __unicode__(self):
        return self.unionid

    def is_token_expired(self):
        return True

    def is_authenticated(self):
        return True

    def is_anonymous(self):
        return self.is_authenticated()



