# -*- coding: UTF-8 -*-

from website.models import FunkesUser
from website.user import User as DUser
from django.contrib.auth import models

class Singleton(object):
      __instance__ = None

      def __new__(cls, *args, **kwargs):
          if not cls.__instance__:
              cls.__instance__ = super(Singleton, cls).__new__(cls, *args, **kwargs)
          return cls.__instance__

class WechatAuth(Singleton):

      def register(self, wechat_user_info, phone, email):
          pass

      def authenticate(self, **kwargs):
          unionid = kwargs.pop('unionid')
          user = FunkesUser.objects.filter(weid=unionid)
          if len(user) == 0:
              user = FunkesUser.objects.create(weid=unionid,
                   name=kwargs['name'],
                   sex=kwargs['sex'],
                   headimgurl=kwargs['headimgurl'])
          else:
              user = user[0]

          return create_user_from_model(user)


      def get_user(self, user_id):
          if user_id != None:
              user = create_user_from_model(FunkesUser.objects.get(pk=user_id))
              return user
          if (hasattr(self, 'request')
                and self.request.session.has_key('user_id')):
              user = create_user_from_model(FunkesUser.objects.get(pk=int(self.request.session['user_id'])))
              return user
          return models.AnonymousUser()

def create_user_from_model(user):
    return DUser(id=int(user.id),
                 openid=user.weid,
                 name=user.name,
                 unionid=user.weid,
                 sex=user.sex,
                 phone=user.phone,
                 email=user.email,
                 province=None,
                 city=None,
                 country=None,
                 headimgurl=user.headimgurl)

def set_session_from_user(request, user):
    request.session['user_id'] = user.id
    # Update the user object cached in the request
    request._cached_user = user
    request.user = user