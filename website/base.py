# -*- coding: UTF-8 -*-

from django.contrib.auth import middleware
from django.contrib import auth
from django.contrib.auth import models
from django.conf import settings

def get_user(request):
    try:
        if request.session.has_key('user_id'):
            user_id = request.session['user_id']
        elif request.COOKIES.has_key('user_id'):
            user_id = request.COOKIES['user_id']
        else:
            raise KeyError
        backend_path = settings.AUTHENTICATION_BACKENDS[0]
        backend = auth.load_backend(backend_path)
        backend.request = request
        user = backend.get_user(user_id)
    except Exception as e:
        user = models.AnonymousUser()
    return user

def middleware_get_user(request):
    if not hasattr(request, '_cached_user'):
        request._cached_user = get_user(request)
    return request._cached_user

def patch_middleware_get_user():
    middleware.get_user = middleware_get_user
    auth.get_user = get_user