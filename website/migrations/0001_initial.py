# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import django.contrib.auth.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('funkes_user_id', models.CharField(default=None, max_length=255, serialize=False, primary_key=True)),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
            },
            bases=(models.Model, django.contrib.auth.models.AnonymousUser),
        ),
        migrations.CreateModel(
            name='ApplyUser',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('weid', models.CharField(default=b'', max_length=100, verbose_name='wechatid')),
                ('actid', models.CharField(default=b'', max_length=100, verbose_name='activateid')),
                ('name', models.CharField(default=None, max_length=50, verbose_name='username')),
                ('sex', models.CharField(default=None, max_length=50, verbose_name='sex')),
                ('phone', models.CharField(default=None, max_length=128, verbose_name='phone')),
                ('email', models.CharField(default=None, max_length=128, verbose_name='email')),
                ('desc', models.CharField(default=None, max_length=400, verbose_name='description')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='created_at')),
                ('updated_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='updated_at')),
            ],
        ),
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=50, verbose_name='title')),
                ('text', models.TextField(max_length=4000, verbose_name='text')),
                ('desc', models.CharField(max_length=400, verbose_name='description')),
                ('likes', models.IntegerField(default=0)),
                ('browse', models.IntegerField(default=0)),
                ('deleted', models.IntegerField(default=0)),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='created_at')),
                ('updated_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='updated_at')),
            ],
        ),
        migrations.CreateModel(
            name='Fanout',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('url', models.CharField(default=b'', max_length=500, verbose_name='url')),
                ('title', models.CharField(default=None, max_length=50, verbose_name='title')),
                ('desc', models.CharField(default=None, max_length=400, verbose_name='description')),
                ('acceptable', models.IntegerField(default=0)),
                ('type', models.CharField(default=None, max_length=50, verbose_name='type')),
                ('finished', models.IntegerField(default=0)),
                ('enddate', models.DateTimeField(default=django.utils.timezone.now, verbose_name='enddate')),
                ('deleted', models.IntegerField(default=0)),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='created_at')),
                ('updated_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='updated_at')),
            ],
        ),
        migrations.CreateModel(
            name='FunkesUser',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('weid', models.CharField(default=b'', max_length=100, verbose_name='wechatid')),
                ('name', models.CharField(default=None, max_length=50, verbose_name='username')),
                ('sex', models.CharField(default=None, max_length=50, verbose_name='sex')),
                ('phone', models.CharField(default=None, max_length=128, verbose_name='phone')),
                ('email', models.CharField(default=None, max_length=128, verbose_name='email')),
                ('headimgurl', models.CharField(default=None, max_length=600, verbose_name='headimgurl')),
                ('desc', models.CharField(default=None, max_length=400, verbose_name='description')),
                ('deleted', models.IntegerField(default=0)),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='created_at')),
                ('updated_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='updated_at')),
            ],
        ),
        migrations.CreateModel(
            name='ScrumItem',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('finished', models.IntegerField(default=0)),
                ('confirmed', models.IntegerField(default=0)),
                ('mediaurl', models.CharField(default=b'', max_length=500, verbose_name='url')),
                ('deleted', models.IntegerField(default=0)),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='created_at')),
                ('updated_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='updated_at')),
                ('fanoutid', models.ForeignKey(to='website.Fanout')),
                ('userid', models.ForeignKey(to='website.FunkesUser')),
            ],
        ),
        migrations.AddField(
            model_name='fanout',
            name='userid',
            field=models.ForeignKey(to='website.FunkesUser'),
        ),
        migrations.AddField(
            model_name='article',
            name='fanoutid',
            field=models.ForeignKey(to='website.Fanout'),
        ),
        migrations.AddField(
            model_name='article',
            name='userid',
            field=models.ForeignKey(to='website.FunkesUser'),
        ),
        migrations.AddField(
            model_name='applyuser',
            name='userid',
            field=models.ForeignKey(to='website.FunkesUser'),
        ),
    ]
