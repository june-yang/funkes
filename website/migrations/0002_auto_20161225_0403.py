# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='applyuser',
            name='desc',
            field=models.CharField(default=None, max_length=400, null=True, verbose_name='description'),
        ),
        migrations.AlterField(
            model_name='applyuser',
            name='email',
            field=models.CharField(default=None, max_length=128, null=True, verbose_name='email'),
        ),
        migrations.AlterField(
            model_name='applyuser',
            name='name',
            field=models.CharField(default=None, max_length=50, null=True, verbose_name='username'),
        ),
        migrations.AlterField(
            model_name='applyuser',
            name='phone',
            field=models.CharField(default=None, max_length=128, null=True, verbose_name='phone'),
        ),
        migrations.AlterField(
            model_name='applyuser',
            name='sex',
            field=models.CharField(default=None, max_length=50, null=True, verbose_name='sex'),
        ),
        migrations.AlterField(
            model_name='article',
            name='desc',
            field=models.CharField(max_length=400, null=True, verbose_name='description'),
        ),
        migrations.AlterField(
            model_name='article',
            name='text',
            field=models.TextField(max_length=4000, null=True, verbose_name='text'),
        ),
        migrations.AlterField(
            model_name='article',
            name='title',
            field=models.CharField(max_length=50, null=True, verbose_name='title'),
        ),
        migrations.AlterField(
            model_name='fanout',
            name='desc',
            field=models.CharField(default=None, max_length=400, null=True, verbose_name='description'),
        ),
        migrations.AlterField(
            model_name='fanout',
            name='title',
            field=models.CharField(default=None, max_length=50, null=True, verbose_name='title'),
        ),
        migrations.AlterField(
            model_name='fanout',
            name='url',
            field=models.CharField(default=b'', max_length=500, null=True, verbose_name='url'),
        ),
        migrations.AlterField(
            model_name='funkesuser',
            name='desc',
            field=models.CharField(default=None, max_length=400, null=True, verbose_name='description'),
        ),
        migrations.AlterField(
            model_name='funkesuser',
            name='email',
            field=models.CharField(default=None, max_length=128, null=True, verbose_name='email'),
        ),
        migrations.AlterField(
            model_name='funkesuser',
            name='headimgurl',
            field=models.CharField(default=None, max_length=600, null=True, verbose_name='headimgurl'),
        ),
        migrations.AlterField(
            model_name='funkesuser',
            name='phone',
            field=models.CharField(default=None, max_length=128, null=True, verbose_name='phone'),
        ),
        migrations.AlterField(
            model_name='scrumitem',
            name='mediaurl',
            field=models.CharField(default=b'', max_length=500, null=True, verbose_name='url'),
        ),
    ]
