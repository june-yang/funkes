/**
 * Created by june-yang on 2017/2/26.
 */

$('.mui-bar.mui-bar-tab > a').on('tap', function() {
    var url = $(this).data('url');
    var content_id = $(this).data('id');
    $('.mui-bar.mui-bar-tab > a').removeClass('mui-active');
    $(this).addClass('mui-active');
    $('.mui-content > div').removeClass('mui-active');
    $('.mui-content #' + content_id).load(url).addClass('mui-active');
});
mui('body').on('shown', '.mui-popover', function(e) {
    //console.log('shown', e.detail.id);//detail为当前popover元素
});
mui('body').on('hidden', '.mui-popover', function(e) {
    //console.log('hidden', e.detail.id);//detail为当前popover元素
});
mui('body').on('tap', '.mui-card-header', function () {
    var url = '/mobile/store/detail/' + $(this).data('id');
    $('#store').load(url);
});

mui('body').on('tap', '.mui-icon-extra.mui-icon-extra-cart', function (){
    var item_id = $(this).data('id');
    var url = '/mobile/store/cart?id=' + item_id;
    $('#control').load(url);
});

mui('body').on('tap', 'a[target="_blank"]', function () {
    var url = $(this).attr('href');
    location.href = url;
});