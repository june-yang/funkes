# -*- coding: UTF-8 -*-
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View
from django.template import loader, Context
from django.contrib.auth import authenticate
from xml.etree import ElementTree as ET
import time
import json
import hashlib
from api.wechat import Wechat
from website.wechatauth import set_session_from_user
from website.models import FunkesUser

@csrf_exempt
def wechat(request):
    if request.method == 'GET':
        signature = request.GET.get('signature', None)
        timestamp = request.GET.get('timestamp', None)
        nonce = request.GET.get('nonce', None)
        echostr = request.GET.get('echostr', None)

        token = 'hkw140i53hwkywm41mm'

        hashlist = [token, timestamp, nonce]
        hashlist.sort()

        hashstr = "%s%s%s" % tuple(hashlist)

        hashstr = hashlib.sha1(hashstr).hexdigest()

        return HttpResponse(echostr, content_type='text')
    if request.method == 'POST':
        str_xml = ET.fromstring(request.body)
        fromUser = str_xml.find('ToUserName').text
        toUser = str_xml.find('FromUserName').text
        content = str_xml.find('Content').text

        nowtime = str(int(time.time()))
        # t = loader.get_template('wechat/text.xml')
        # c = Context({'toUser': toUser, 'fromUser': fromUser,
        #        'nowtime': nowtime, 'content': 'hello world'})
        return HttpResponse('<xml> \
                            <ToUserName><![CDATA[%s]]></ToUserName> \
                            <FromUserName><![CDATA[%s]]></FromUserName> \
                            <CreateTime>%s</CreateTime> \
                            <MsgType><![CDATA[text]]</MsgType> \
                            <Content><!CDATA[%s]]></Content> \
                            </xml>' % (toUser, fromUser, nowtime, 'hello world!'), content_type='application/xml;')
    return HttpResponse('method not supported!', content_type='text')
    #if hashstr == signature:
    #    return HttpResponse(echostr)

    #return HttpResponse(json.dumps({'hashstr': hashstr, 'signature': signature, 'echostr': echostr}), content_type='application/json;')

@csrf_exempt
def wxlogin(request):
    code = request.GET['code']
    state = request.GET['state']
    wechat_session = Wechat()
    access_token = wechat_session.get_user_access_token(code)
    user_info = {}
    if access_token.get('errcode', 0) == 0:
        user_info = wechat_session.get_user_info(access_token['openid'])
        print user_info
        user = authenticate(request=request,
                     unionid=user_info['openid'],
                     name=user_info['nickname'],
                     sex=user_info['sex'],
                     headimgurl=user_info['headimgurl'])
        set_session_from_user(request, user)
    redirect_url = '/wxprofile'
    if state == '2':
        redirect_url = '/store/'
    return HttpResponseRedirect(redirect_url)

@csrf_exempt
def user_patch(request):
    id = int(request.user.id)
    user = FunkesUser.objects.get(pk=id)
    _user = json.loads(request.body)
    user.phone = _user['phone']
    user.email = _user['email']
    print _user
    user.save()
    return HttpResponse('success')