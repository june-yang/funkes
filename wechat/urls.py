# -*- coding: UTF-8 -*-
from django.conf.urls import url
from django.conf.urls import patterns

urlpatterns = patterns(
    'wechat.views',
    url(r'^$', 'wechat'),
    url(r'^user_patch/', 'user_patch'),
    url(r'^login$', 'wxlogin'),
)
