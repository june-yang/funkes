"""hiall URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^', include('website.index.urls')),
    url(r'^store/', include('website.store.urls')),
    url(r'^wechat/', include('wechat.urls')),
    url(r'^mobile/', include('website.mobile.urls')),
    url(r'^(?P<path>.txt)$', 'django.views.static.serve',
       {'document_root': 'static/', 'show_indexes': True}),
    url(r'^api/$', include('api.urls')),
]
