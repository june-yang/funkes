# -*- coding: UTF-8 -*-
from django.conf.urls import url
from django.conf.urls import patterns
from api.v1.auth import AuthController
from api.v1.users import UserController

__AVAIL_API__ = [
    UserController
]

# auth controller required
urlpatterns = patterns(
    '',
    url(r'^auth$', AuthController.as_view()),
)

for _api in __AVAIL_API__:
    if _api.name:
        urlpatterns += patterns('', url(r'^%s/%s' % (_api.namespace, _api.name), _api.as_view()))
        urlpatterns += patterns('', url(r'^%s/%s/(?P<pk>\d+)' % (_api.namespace, _api.name), _api.as_view()))
