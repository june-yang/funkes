from django.views.generic import View
from django.http import Http404

class BaseController(View):
    namespace = 'funkes'
    name = None
    def get(self, request, *args, **kwargs):
        pass

    def post(self, request, *args, **kwargs):
        pass

    def put(self, request, *args, **kwargs):
        pass

    def delete(self, request, *args, **kwargs):
        pass

    def index(self, request, *args, **kwargs):
        pass