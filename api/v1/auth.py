# -*- coding: UTF-8 -*-
from api.apibase import BaseController
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt


class AuthController(BaseController):

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        return HttpResponse('success', content_type='application/json;')