# -*- coding: UTF-8 -*-

import requests
import json

_appid = 'wx9e519842ae6dcbcd'
_secret = '198dc2e4a2f685501028715f9fcd0c7a'
_token_url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s'
_user_info_url = 'https://api.weixin.qq.com/cgi-bin/user/info?access_token=%s&openid=%s&lang=zh_CN '
_user_info_access_token = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code'


class Singleton(object):
      __instance__ = None

      def __new__(cls, *args, **kwargs):
          if not cls.__instance__:
              cls.__instance__ = super(Singleton, cls).__new__(cls, *args, **kwargs)
          return cls.__instance__

class Wechat():
    __token__ = None

    def __init__(self):
        self.session = requests.Session()
        if not self.__token__:
            response = self.session.get(_token_url % (_appid, _secret))
            self.__token__ = json.loads(response.content)

    def __del__(self):
        if self.session:
            self.session.close()
        self.__token__ = None

    def get_user_access_token(self, code):
        access_token = self.session.get(_user_info_access_token % (_appid, _secret, code))
        return json.loads(access_token.content)

    def get_user_info(self, openid):
        user_info = self.session.get(_user_info_url % (self.__token__['access_token'], openid))
        return json.loads(user_info.content)
